# Setup 6

~~The voyage .. yeah we know..~~

## Create the setup6 k8s cluster

It's a little different this time:

```sh
source ./setup.sh
```

This will setup a standard kubernetes setup ready for us to some troubelshooting with.

| WARNING: Please do not look into the `.donotlook` folder. Also do not look into the `setup6.yml`. You would be seeing things about the setup that would give it all away ;-) |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

## Extra requirements

We are going to work with contexts this time.

You can view the "contexts" or "users" you have access to with:

```sh
kubectl config get-contexts
```

You can them switch between them using:

```sh
kubectl config use-context [NAME OF CONTEXT HERE]
```

## Deploying our super app

```sh
kubectl apply -f expose.yml
kubectl apply -f deployment.yml
```

And now we need to try and find out what I broke/did wrong, and how to then go and fix it!

# Question from the customer

I can deploy those yaml file with the Admin account, but that is really unsafe, so we are trying to deploy things
using the `workshop` user, but we are having trouble understanding RBAC.

Can you help us create a minimal RBAC yaml that will allow the workshop user to deploy just these things?

# Solution

<details>
  <summary>Spoiler warning, solution to setup6</summary>

I'm so sorry, this one must have been hard... again...

The main tool I use for all things "RBAC" is [RBAC tool](https://github.com/alcideio/rbac-tool).

We can easily install it with:

```sh
kubectl krew install rbac-tool
```

Now we can start some investigating:

```sh
kubectl rbac-tool whoami
```

That shows us who we are according to the cluster. It seems we are the user "workshop".
Ok, So what can user "workshop" do in this cluster:

```sh
kubectl rbac-tool policy-rules workshop
```

Ok. so that's.. not a lot ;-)

Let's see what "contexts" are available in our kubeconfig:

```sh
kubectl config get-contexts
```

As you can see, there are two, and we are currently on the workshop context.

We can switch to the other context like so:

```sh
kubectk config use-context k3d-setup6
```

And when we do the `whoami` and the `policy-rules` command again, we can see that we
are the admin of this cluster now.

Using `rbac-tool` we can now start to create a new RBAC role with all the permissions
we might want. For instance:

```sh
❯ kubectl rbac-tool generate --allowed-groups=, --generated-type=role
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  annotations: null
  creationTimestamp: null
  labels: null
  name: custom-role
  namespace: mynamespace
rules:
- apiGroups:
  - ""
  resources:
  - bindings
  - componentstatuses
  - configmaps
  - resourcequotas
  - nodes
  - services
  - events
  - persistentvolumeclaims
  - endpoints
  - pods
  - serviceaccounts
  - namespaces
  - persistentvolumes
  - replicationcontrollers
  - secrets
  - limitranges
  - podtemplates
  verbs:
  - '*'
```

save it as a YAML, then remove what you do not need, and presto! New rbac role.

Try out the generate, show and analysis commands of this tool!

</details>
