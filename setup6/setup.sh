#!/bin/bash
#

k3d cluster delete -c setup6.yml

sudo rm -rf .donotlook/tls/*
unzip .donotlook/tls.zip -d .donotlook/ >/dev/null 2>&1 

k3d cluster create -c setup6.yml

export KUBECONFIG=workshop.yaml
kubectl config use-context k3d-setup6-workshop
