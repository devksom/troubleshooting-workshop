# Setup 0

How to "kubernetes" ;-)

## Create the setup0 k8s cluster

```sh
k3d cluster create -c setup0.yml
```

This will setup a standard kubernetes setup in which we can install our demo application.

## Deploying mysql and wordpress

```sh
kubectl apply -f secret.yml
kubectl apply -f mysql.yml
kubectl apply -f wordpress.yml
```

# Question from the customer

I would like to know how to deploy a program / website to kubernetes.

