# Troubleshooting workshop

## Intro

This repo provides a few different kubernetes setups, each with a different
problem / situation to troubleshoot and fix.

Each folder provides a setup yaml for [ k3d ](https://k3d.io/v5.4.9/) (which is
what we use to quickly and repeatably setup kubernetes clusters on local
machines such as laptops).

Each setup folder also has a readme with some extra instructions (if needed), so
make sure to check it!

The solution to each setup can be found on the bottom of each readme. Luckily
it's marked as a spoiler, so you need to click on it to expand it. But just try
and enjoy the exploration first ;-)

Then there's the miscelaneous folder, containing some extra information that
might help in general troubleshooting of Kubernetes clusters.

## Requirements:

1. [rancher desktop](https://rancherdesktop.io/) (in [ Moby
   ](https://docs.rancherdesktop.io/ui/preferences/container-engine) mode,
   meaning: providing dockerd) or another method to supply docker engine on your
   laptop.
2. [k3d](https://k3d.io/) to be able to install and manage kubernetes clusters
3. A few tools that may or may not be used ;-)
   - kubectl
   - [ krew ](https://krew.sigs.k8s.io/)
   - [ asdf ](https://asdf-vm.com/) (personal favorite to install all kinds of
     tools)
