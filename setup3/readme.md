# Setup 3

The voyage .. yeah we know..

## Create the setup3 k8s cluster

```sh
k3d cluster create -c setup3.yml
```

This will setup a standard kubernetes setup ready for us to break, and then fix again :-)
After creating the cluster, we need to break it by running the next step:

## Deploying our super app

```sh
kubectl apply -f pvc.yml
kubectl apply -f deployment.yml
```

And now we need to try and find out what I broke/did wrong, and how to then go and fix it!

# Question from the customer

I have a deployment that uses a PVC, but the pods won't start!
We get a weird message:

```
0/4 nodes are available: pod has unbound immediate PersistentVolumeClaims. preemption: 0/4 nodes are available: 4 No preemption victims found for incoming pod..
```

# Solution

<details>
  <summary>Spoiler warning, solution to setup3</summary>

This one is about storageclass(es) and the fact we are not using the correct one.
We are asking the cluster to provide us with a storage based on the "default" storageclass. But when we look at the list of storageclasses:

```sh
k get sc
```

We see that our list of storageclasses only has:

```
❯ k get sc
NAME                   PROVISIONER             RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
local-path (default)   rancher.io/local-path   Delete          WaitForFirstConsumer   false                  25m
```

one class. the "local-path".

We cannot just change this by the way. This flag is immutable as far as the cluster is concerned. So you will have to delete the PVC, change the yaml and apply the yaml again.

Or, just leave out the entire "storageClassName" rule to default back to the standard storageclass in the cluster ;-)

## Commands

```sh
kubectl describe pvc/[pvcname]
kubectl get sc # or `kubectl get storageclasses` since `sc` is just a short version of that CRD.
```

The describe of the PVC will already show you what's wrong.

</details>
