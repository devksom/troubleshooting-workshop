# api-resources

Sometimes you might want to know what CRD's or "API" calls like "deploy" or "pvc" there are in the cluster.

With `kubectl api-resources` you will get a list of all of them including the shorter names of it.

For instance:

```sh
❯ k api-resources | grep -i sc
horizontalpodautoscalers          hpa          autoscaling/v2                         true         HorizontalPodAutoscaler
endpointslices                                 discovery.k8s.io/v1                    true         EndpointSlice
flowschemas                                    flowcontrol.apiserver.k8s.io/v1beta3   false        FlowSchema
ingressclasses                                 networking.k8s.io/v1                   false        IngressClass
priorityclasses                   pc           scheduling.k8s.io/v1                   false        PriorityClass
storageclasses                    sc           storage.k8s.io/v1                      false        StorageClass
```
