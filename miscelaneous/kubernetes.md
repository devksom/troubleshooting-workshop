# So what makes Kubernetes so great

That's the question right?

A few years ago, software was mostly monolithic, large, and bulky. Everything was contained in one executable.

Then came microservices, splitting the monolithic stuff up in to multiple smaller parts.

We then put those smaller parts in their own little environment (containers) that we where easily able to run and maintain.

So now we needed a way to manage all these smaller parts of a greater whole. We needed a controller to manage all those individual containers, a way to make them talk to and find each other.

Kubernetes is the current result of that desire / search for managing containers.

## Parts of a cluster (technically speaking)

The basic building blocks are:

1. Api server
2. Controller manager
3. Scheduler
4. etcd (sometimes a different state storage)


Where the api is what your web interface, automation tooling and the command line tool `kubectl` uses;

The controller checks all the parts and that they still work (reacts when a node goes down, etc) and;

The scheduler is tasked with figuring out where a container can start, as in: on which node(s).

And last (and most annoying, when it breaks) the etcd. It holds the current state of the cluster. All the yaml you've sent to the cluster is stored here (as json ofcourse, but you get my point).

## Parts of a cluster (programatically speaking)

The most basic "thing" in Kubernetes is the:

1. pod.

Yeah, that's really all there is in Kubernetes. It's all about the pod. All other things are ways to work with/assist/store data for/etc, etc with that basic building block.

If you understand the pod, you will understand the "red line" that pervades through all that is: Kubernetes. We it-folk always want to know that part first right? The rest will follow... using google ;-)

So what is a "pod"?  A pod is one or more containers that are tightly grouped together (They are absolutely co-dependent on each other). Each container in that pod can reach another container in the same pod via the 127.0.0.1 localhost address.

Yes, all the containers in the pod share the same network space and sorta, kinda act like a VM would in that sense.

Now, you can run multiple pods, and each will get an ip address assigned. And the containers can then reach each other via those ip addresses. pretty basic stuff right?

## services

Well, containers get stopped and started all the time. Meaning they get new ip addresses all the time.

That's why you will want to use a "service" for reaching each other. Services are always available under the same DNS name. See, we now know the first extra "thing" that we can bolt on top of the pod. A "service".

P.s. A service also acts as a loadbalancer if two or more pods are attached to it.

## configmaps

But, what if the dns name of that service changes? Well, let's assume that the program looks at the environment variable "${DBHOST}" for the service name where it can find it's Database Host

The environment variable can be filled with data (key=value) from a configmap.

Configmaps are quite capable things we can bolt on top of... pods.

It can hold the contents of entire config files, and we can mount that inside the pod.

Or we can use it as a key / value store and use it to fill environment variables inside the pods.

## secrets

same as configmaps really. Supposed to hold secrets. But they are base64 encoded. so not really secret ;-)

You are supposed to use a third party service (like hashicorp vault) to manage the secrets in a ... secret... way.

## Deployments

Describes a collection of pods, how many you want to run, etc. 

## Daemon set

Describes a pod that will run "x" amount of times on each node in the cluster. Usually used for monitoring software for instance.

## Ingress

Reverse proxy basically. But a really cool one. Always attaches to a service.

## PVC
Persistent Volume Claim.

Hard drive; and bolt it onto a ... pod.


# Summary

The above is the bread and butter stuff of a kubernetes cluster, there's more ofcourse; but it starts to become more specialised and esotheric the more you get into the weeds ;-)

Ya got replica sets, endpoints, RBAC, ingressclasses, namespaces, etc.

Just remember: it's all about the pod. Managing access to it, either via services or RBAC rules, how many times it needs to run or grouped together in a namespace.