# explain

So there are all kinds of options in a "yaml" right? So how can you know what you can use and what the options are?

That's what `kubectl explain` is for.

```sh
❯ k explain pod.spec.volumes.nfs
KIND:     Pod
VERSION:  v1

RESOURCE: nfs <Object>

DESCRIPTION:
     nfs represents an NFS mount on the host that shares a pod's lifetime More
     info: https://kubernetes.io/docs/concepts/storage/volumes#nfs

     Represents an NFS mount that lasts the lifetime of a pod. NFS volumes do
     not support ownership management or SELinux relabeling.

FIELDS:
   path <string> -required-
     path that is exported by the NFS server. More info:
     https://kubernetes.io/docs/concepts/storage/volumes#nfs

   readOnly     <boolean>
     readOnly here will force the NFS export to be mounted with read-only
     permissions. Defaults to false. More info:
     https://kubernetes.io/docs/concepts/storage/volumes#nfs

   server       <string> -required-
     server is the hostname or IP address of the NFS server. More info:
     https://kubernetes.io/docs/concepts/storage/volumes#nfs
```

Shows you what the `nfs` option can do in the pod.spec.volumes section.

# explore / explain

Even better to "explain" is to install:

```sh
kubectl krew install explore
```

You will need to install krew first, but it has soooo many cool plugins:

- https://krew.sigs.k8s.io/plugins/

With `kubectl explore` you can now interactively scroll through the options in for instance:

![explore screenshot](images/explore.png)
