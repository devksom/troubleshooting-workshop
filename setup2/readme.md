# Setup 2

The voyage continues!

## Create the setup2 k8s cluster

```sh
k3d cluster create -c setup2.yml
```

This will setup a standard kubernetes setup ready for us to break, and then fix again :-)
After creating the cluster, we need to break it by running the next step:

## Deploying our super app

```sh
kubectl apply -f deployment.yml
kubectl apply -f role.yml
kubectl apply -f psp.yml
```

And now we need to try and find out what I broke/did wrong, and how to then go and fix it!

# Question from the customer

So I have a test deployment that wants to run as root. And we do not want that to ever happen!
So we created a Policy to make sure no pods could ever start as root. But we are having trouble
applying that `psp.yml`. Everything looks great, we have used this Policy in the past so we **know**
policy is perfectly fine..

Yet it won't apply to the cluster!

# Solution

<details>
  <summary>Spoiler warning, solution to setup2</summary>

Yeah this is a sneaky one. Simply put: `psp's` or `PodSecurityPolicies` have been deprecated for a while now.
And this version of kubernetes simply no longer supports `psp's`.

So it makes sense that "it used to work fine": It was a different kubernetes version.

Deprecations in Kubernetes happen all the time, and you really need to pay attention when upgrading
to make sure all your stuff will still work in the same way.
Great reason to NEVER upgrade in place, but ALWAYS move to a fresh new cluster.

The replacement for `PSP's` is the `PodSecurity Admission`.

- https://kubernetes.io/docs/concepts/security/pod-security-admission/

So what are the commands we could have used to get to this realisation and what can we do to help the customer
in their noble fight from stopping bad root pods to ever start/deploy on their clusters?

## Commands

To be honest, this should be enough:

```sh
kubectl apply -f psp.yml
```

This produces the error:

```
error: resource mapping not found for name: "norootcontainers" namespace: "" from "./psp.yml": no matches for kind "PodSecurityPolicy" in version "policy/v1beta1"
ensure CRDs are installed first
```

It's the `no matches for kind "PodSecurityPolicy" in version "policy/v1beta1"` part that gives it away. This type of error is givven when the cluster has no idea what to do with your command.
That is also the reason why it says: `ensure CRDs are installed first`. It's suggesting that perhaps you have not yet installed the tool that can handle your request.

Like you first need to add trident/netapp support before you can request volumes using that software.

## PodSecurity Admission Controller

Instead of roles, rolebindings and psp's we can add this to our namespace (labels):

```sh
kubectl label --overwrite ns default pod-security.kubernetes.io/enforce=restricted
kubectl rollout restart deploy/setup2
```

You can find more about this here:

- There are three levels your can choose: https://kubernetes.io/docs/concepts/security/pod-security-standards/
- More examples: https://kubernetes.io/docs/tasks/configure-pod-container/enforce-standards-namespace-labels/

This restriced security policy is really ... restrictive. It will probably complain about a lot more then just running as root ;-)
But the error messages are clear enough to start adjusting your deployment to suit this new security policy:

```
Error creating: pods "setup2-6d8c46464f-7wh85" is forbidden:
  violates PodSecurity "restricted:latest":
    allowPrivilegeEscalation != false (container "setup2" must set securityContext.allowPrivilegeEscalation=false),
    unrestricted capabilities (container "setup2" must set securityContext.capabilities.drop=["ALL"]),
    runAsNonRoot != true (pod or container "setup2" must set securityContext.runAsNonRoot=true),
    runAsUser=0 (pod must not set runAsUser=0),
    seccompProfile (pod or container "setup2" must set securityContext.seccompProfile.type to "RuntimeDefault" or "Localhost")
```

</details>
