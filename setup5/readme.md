# Setup 5

~~The voyage .. yeah we know..~~

## Create the setup5 k8s cluster

```sh
k3d cluster create -c setup5.yml
```

This will setup a standard kubernetes setup ready for us to break, and then fix again :-)
After creating the cluster, we need to break it by running the next step:

## Extra requirements

We are going to visit a https site, so it would be nice if we had a TLS certificate that works in our browser.
It is not strictly neccesary for this setup, but it is really nice

Installing on ubuntu:

```sh
sudo apt install libnss3-tools mkcert
```

We then install the `mkcert` keys on our system:

```sh
mkcert -install
mkcert setup5.127.0.0.1.sslip.io
kubectl create secret tls setup5-tls \
  --cert=./setup5.127.0.0.1.sslip.io.pem \
  --key=./setup5.127.0.0.1.sslip.io-key.pem
```

## Deploying our super app

```sh
kubectl apply -f expose.yml
kubectl apply -f deployment.yml
```

And now we need to try and find out what I broke/did wrong, and how to then go and fix it!

# Question from the customer

Ok, sure you repaired our stuff in setup4, but somehow we changed nothing and things are broken again!
We checked the https->http stuff and that is fine this time.

Why are we not reaching our app?

p.s. You should be able to reach the app on:

- https://setup5.127.0.0.1.sslip.io:8445

# Solution

<details>
  <summary>Spoiler warning, solution to setup5</summary>

I'm so sorry, this one must have been hard...
Everything looked fine right? Well if everything looks fine, perhaps it's the docker container
itself that is not configured correctly.

The solution in a nutshell: The nginx daemon in the docker container is not
listening on port 80... But on some other port.

## Which port?

There are two ways to find out:

1. We start a debugging container. Which is a second docker container that get's
   started in the same namespace as the container which this debug container is
   started on.

So for instance:

```sh
kubectl debug -it setup5-699767f8cc-dgtfq --image=debian
```

And when the debian container starts we can install stuff like:

```sh
apt update
apt install net-tools
netstat -tulpen
```

That last command will show you the ports on which stuff is listening. Bingo.

The other INSANELY cool thing is to attach tcpdump to the container and stream all the network
traffic through wireshark on your own machine.

So: install wireshark and:

```sh
kubectl krew install sniff
kubectl sniff setup5-699767f8cc-brxq2 -n default
```

This will then setup tcpdump and automatically startup wireshark for you.

After a few refreshes of the webpage, you should see some traffic trying to hit the pod.
If you know how to use Wireshark, you will see the problem.

</details>
