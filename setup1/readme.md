# Setup 1

The voyage begins!

## Create the setup1 k8s cluster

```sh
k3d cluster create -c setup1.yml
```

This will setup a standard kubernetes setup ready for us to break, and then fix again :-)
After creating the cluster, we need to break it by running the next step:

## Deploying our super app

```sh
kubectl apply -f deployment.yml
```

And now we need to try and find out what I broke, and how to then go and fix it!

# Question from the customer

Why in the name of zeus does my second pod not start? You guys suck!

# Solution

<details>
  <summary>Spoiler warning, solution to setup1</summary>

The deployment asks our cluster to run two pods (replica's is set to two) on any node with a `gpu=nvidia` label.
It also wants to put only 1 of such pods on each node with such a tag.

And since only 1 node has that tag, it does not have a node left to put the second pod.

Commands you might have used:

```sh
kubectl get pods
kubectl describe pod [pod name that is status pending]
```

You would then see:

```
0/3 nodes are available:
  1 node(s) didn't match pod anti-affinity rules,
  2 node(s) didn't match Pod's node affinity/selector.

preemption: 0/3 nodes are available:
  1 No preemption victims found for incoming pod,
  2 Preemption is not helpful for scheduling.
```

(I've split the lines up for easier reading)

The first three lines of that describe tell you that there are no nodes to "satisfy" our request for how we want to schedule a new pod/container.
That could be because we wanted more CPU or Memory than any node has available. That is an often recurring problem.

In this case it say's that 1 node cannot be used because of `podantiaffinity` and two nodes did not satisfy the totality of affinity and node selector rules.

That's a bit of a bad error in my opinion since one node is obviously capable of running our pod. But let's look at the Affinity section of our deployment:

```yaml
affinity:
  nodeAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
      nodeSelectorTerms:
        - matchExpressions:
            - key: gpu
              operator: In
              values:
                - nvidia
  podAntiAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
            - key: app
              operator: In
              values:
                - setup1
        topologyKey: "kubernetes.io/hostname"
```

Ah! The deployment states that we want to run our pods on a node that has the label "gpu=nvidia" (nodeAffinity),
but it also states that we do NOT want to start another pod on a node that already has a pod with label "app=setup1" running on it (podAntiAffinity).

Assuming the second node simply does not have an Nvidia GPU, it simply makes no sense to run two pods in this cluster, so lets just reset the number of replica's to 1.

Oh! What is pre-emption? Let's assume we have two pods that have the same demand for a node (CPU, memory, tags) then the one with higher "pre-emption" wins and gets scheduled.
It's a ranking system to influence what decision the kubernetes cluster will make in such situations

https://kubernetes.io/docs/concepts/scheduling-eviction/pod-priority-preemption/

</details>
