# Setup 4

The voyage .. yeah we know..

## Create the setup4 k8s cluster

```sh
k3d cluster create -c setup4.yml
```

This will setup a standard kubernetes setup ready for us to break, and then fix again :-)
After creating the cluster, we need to break it by running the next step:

## Extra requirements

We are going to visit a https site, so it would be nice if we had a TLS certificate that works in our browser.
It is not strictly neccesary for this setup, but it is really nice

Installing on ubuntu:

```sh
sudo apt install libnss3-tools mkcert
```

We then install the `mkcert` keys on our system:

```sh
mkcert -install
mkcert setup4.127.0.0.1.sslip.io
kubectl create secret tls setup4-tls \
  --cert=./setup4.127.0.0.1.sslip.io.pem \
  --key=./setup4.127.0.0.1.sslip.io-key.pem
```

## Deploying our super app

```sh
kubectl apply -f expose.yml
kubectl apply -f deployment.yml
```

And now we need to try and find out what I broke/did wrong, and how to then go and fix it!

# Question from the customer

So we have a pod listening on port 80, a service that listens for 80 and forwards to port 80 of the pod(s) and a ingress
that listens on port 443 and 80 and connects with the service. We still get an error and we now the ports are correct?

The link you can use for this setup: https://setup4.127.0.0.1.sslip.io:8444/
When you fix the issue, you will be able to view the page

# Solution

<details>
  <summary>Spoiler warning, solution to setup4</summary>

Hint: You can name ports, and when you do they take precedence over ports

If you look at the service, you'll see it is trying to forward to port 80 of the pods, but it's called "https" instead of how the pods call that port: "http".

After that change to http it should work.

p.s. Try refreshing the page a couple of times and you will see the loadbalancing in action.

## Commands

```sh
k get svc
k get ing
k edit svc/setup4
```

</details>
